# Enigma Metrics Extension

## Description

An extension for the game Enigma that captures metrics about the user's play, and adds additional components to the play-flow that the user must complete during play.

## Building

Follow the README in the top directory. The Makefile currently assumes that metrics should be built too.

## Packaging

I used checkinstall to build .debs: [checkinstall](https://wiki.debian.org/CheckInstall)

## Installing

Download the install script [here](https://enr0n.net/owncloud/index.php/s/MYGnbRnToSr9YmK?path=%2FLinux).

Then,
```bash
chmod +x install
sudo ./install
```

Run the game: `enigma`
