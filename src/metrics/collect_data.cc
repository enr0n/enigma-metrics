/*
 * Copyright (C) 2018 Nicholas Rosbrook
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include "collect_data.hh"
#include "generate_metrics.hh"
#include "utils.hh"
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <string>

#ifdef WIN32
#include <windows.h>
#endif

namespace metrics {

#ifdef WIN32
std::string DATA_FILE = "C:" + getenv("HOMEPATH") + "\\EnigmaMetrics\\metrics_data.txt";
#else
std::string DATA_FILE =  std::string(getenv("HOME")) + "/.enigma_metrics/metrics_data.txt";
#endif

/**
 * levels_played keeps track of the total number of levels
 * attempted, while levels_completed tracks only the number
 * of levels that are successfully completed
 */
static int levels_played = 0;
static int levels_completed = 0;

/* Keeps track of the status of the session */
static bool session_complete = false;

void createMetricsDirectory() {
	/*
	 * Create the EnigmaMetrics directory. If on Linux,
	 * just use /tmp it will only be tested on Linux. On Win32
	 * we need to create a folder in %HOMEPATH%
     *
     * FIXME: Now targeting linux, and will use a directory like this.
     *        For now, the bash install script creates this directory.
	 */
#ifdef WIN32
	std::string dir("C:" + getenv("HOMEPATH") + "\\EnigmaMetrics");
	CreateDirectory(dir.c_str(), NULL);
#endif

}

void updateDataSuccess(int level_time, std::string user_name, bool under_par, int par_time) {
    /* Create the directory - does nothing on Linux */
    createMetricsDirectory();

    /* Update levels played and levels completed */
    metrics::updateLevelsPlayed(true);

    /* Create and open the data file */
    std::ofstream data_file;
    data_file.open(DATA_FILE.c_str(), std::ios_base::app);

    /* Check if player did better than par */
    std::string level_par = "No";
    if (under_par)
        level_par = "Yes";

    /* Write data to the file */
    data_file << user_name << ":LevelTime:" << level_time << std::endl;
    data_file << user_name << ":ParTime:" << par_time << std::endl;
    data_file << user_name << ":LevelPar:" << level_par << std::endl;
    data_file.close();
}

void updateDataFail(std::string user_name) {
    /* Create the directory - does nothing on Linux */
    createMetricsDirectory();

    /* Update levels played, but not levels completed */
    metrics::updateLevelsPlayed(false);

    /* Create and open the data file */
    std::ofstream data_file;
    data_file.open(DATA_FILE.c_str(), std::ios_base::app);

    /**
    * Write data to the file - since the player failed the level,
    * all of the values will be 'Aborted'
    */
    data_file << user_name << ":LevelTime:Abort" << std::endl;
    data_file << user_name << ":ParTime:Abort" << std::endl;
    data_file << user_name << ":LevelPar:Abort" << std::endl;
    data_file.close();
}

void updateLevelsPlayed(bool success) {
    levels_played++;
    if (success)
        levels_completed++;
}

void sessionFinished(std::string user_name) {
    /**
     *  Write all necessary data to the metrics file
     *  when the user exits the game.
     */

    /**
     *  In case the user already quit from the pre-level
     *  prompt, we don't want to write these metrics again
     */
    if (session_complete)
        return;
    session_complete = true;

    /* Stop the session timer */
    metrics::stopTimer();

    std::ofstream data_file;
    data_file.open(DATA_FILE.c_str(), std::ios_base::app);

    data_file << user_name << ":LevelsPlayed:" << levels_played << std::endl;
    data_file << user_name << ":LevelsCompleted:" << levels_completed << std::endl;
    data_file << user_name << ":TimeElapsed:" << metrics::getElapsedTime() << std::endl;
    data_file << user_name << ":DateTime:" << metrics::getDateTime() << std::endl;
    data_file.close();

    /* Generate the spreadsheet */
    metrics::generateMetrics();
}

int getLevelsPlayed() {
    return levels_played;
}
}//metrics
