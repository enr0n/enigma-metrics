/*
 * Copyright (C) 2018 Nicholas Rosbrook
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include <string>

namespace metrics {

/*----------Functions----------*/
void createMetricsDirectory();
void updateDataSuccess(int level_time, std::string user_name, bool under_par, int par_time);
void updateDataFail(std::string user_name);
void updateLevelsPlayed(bool success);
void sessionFinished(std::string user_name);
int getLevelsPlayed();
} //namespace metrics
