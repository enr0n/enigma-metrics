/*
 * Copyright (C) 2018 Nicholas Rosbrook
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

#include "level_start.hh"
#include <string>
#include <Python.h>

namespace metrics {

int preLevelPrompt(int levels_played, std::string user_name) {
    /* Return status - initialize at fail */
    int status = -1;

    /* Initialize PyObjects */
    PyObject *pName, *pModule, *pDict, *pFunc;
    PyObject *pArgs, *pValue;

    Py_Initialize();

    /* Need to be able to import rating module */
    PyRun_SimpleString("import sys");
    PyRun_SimpleString("sys.path.append(\".\")");

    /* Set module name*/
    pName = PyString_FromString("enigma_metrics.LevelStart");

    /* Load python module */
    pModule = PyImport_Import(pName);
    Py_DECREF(pName);

    if (pModule != NULL) {
        pFunc = PyObject_GetAttrString(pModule, "run");

        if (pFunc && PyCallable_Check(pFunc)) {
            /* Setup pArgs */
            pArgs = PyTuple_New(2);

            /* Set levels played */
            pValue = PyInt_FromLong(levels_played);
            PyTuple_SetItem(pArgs, 0, pValue);

            /* Set user name */
            pValue = PyString_FromString(user_name.c_str());
            PyTuple_SetItem(pArgs, 1, pValue);

            /* Call the python code */
            pValue = PyObject_CallObject(pFunc, pArgs);

            Py_DECREF(pArgs);

            /* Check for success of function call */
            if (pValue == NULL) {
                Py_DECREF(pFunc);
                Py_DECREF(pModule);
                PyErr_Print();
                return -1;
            } else {
                /* Save the status */
                status = (int)PyInt_AsLong(pValue);
            }

        } else {
            /* Cannot find function */
            if (PyErr_Occurred())
                PyErr_Print();
        }
        Py_XDECREF(pFunc);
        Py_DECREF(pModule);

    } else {
        /* Failed to load module */
        PyErr_Print();
        return -1;
    }
    Py_Finalize();
    return status;
}
}//metrics
