# Copyright (C) 2018 Nicholas Rosbrook
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#

from enigma_metrics import METRICS_DATA_FILE, METRICS_WORKBOOK

import os
import xlsxwriter
import datetime

# List of keys for all metrics
metrics_keys = [ 'User',
                 'LevelsPlayed',
                 'LevelsCompleted',
                 'LevelTime',
                 'ParTime',
                 'LevelPar',
                 'KeypressTime',
                 'Rating',
                 'Difficulty',
                 'TimeElapsed',
                 'DateTime' ]

# List of metrics that have one entry per player
player_metrics = [ 'User',
                   'LevelsPlayed',
                   'LevelsCompleted',
                   'TimeElapsed',
                   'DateTime' ]

# Column headers for player metrics
player_metrics_headers = { player_metrics[0]: 'Name',
                           player_metrics[1]: 'Levels Played',
                           player_metrics[2]: 'Levels Completed',
                           player_metrics[3]: 'Duration',
                           player_metrics[4]: 'Date Played' }

# List of metrics that have entries per-level
level_metrics = [ 'LevelTime',
                  'ParTime',
                  'LevelPar',
                  'KeypressTime',
                  'Rating',
                  'Difficulty' ]

# Column headers for level metrics
level_metrics_headers = { level_metrics[0]: 'Level Time',
                          level_metrics[1]: 'Par Time',
                          level_metrics[2]: 'Met Par?',
                          level_metrics[3]: 'Key-press Time',
                          level_metrics[4]: 'Enjoyment Rating',
                          level_metrics[5]: 'Difficulty Rating' }

def writeMetrics():
    """ Create a worksheet from data file """
    # Write user data
    data = readData()

    # Get the highest number of levels played
    # determine how many columns to write in header
    max_levels = getMaxLevels(data)

    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook(METRICS_WORKBOOK)
    worksheet = workbook.add_worksheet()

    # Set the column headers
    writeColumnHeaders(worksheet, max_levels)

    row = 1
    for player_id in data.keys():

        # Write the player metrics first
        num_player_metrics = len(player_metrics)
        for i in range(num_player_metrics):
            key = player_metrics[i]
            value = data[player_id][key][0]
            worksheet.write(row, i, value)

        # Write the level metrics
        num_level_metrics = len(level_metrics)

        # Iterate over all metric types
        for i in range(num_level_metrics):

            # Start at the correct column
            col = num_player_metrics + i

            # Iterate over all values of this type
            key = level_metrics[i]
            for value in data[player_id][key]:
                worksheet.write(row, col, value)

                # Make sure to jump to the correct next spot
                col += num_level_metrics

        # Go to next row for the next user
        row += 1

def writeColumnHeaders(worksheet, num_levels):
    """ Write the column headers """

    # Write the column headers for player metrics
    num_player_metrics = len(player_metrics)
    for i in range(num_player_metrics):
        header = player_metrics_headers[player_metrics[i]]
        worksheet.write(0, i, header)

    # Write column headers for level metrics
    num_level_metrics = len(level_metrics)
    num_levels = num_level_metrics * num_levels + 1
    for i in range(num_player_metrics, num_levels, num_level_metrics):
        for j in range(num_level_metrics):
            header = level_metrics_headers[level_metrics[j]]
            worksheet.write(0, i+j, header)

def readData():
    """ Retrieve entries in data file """
    if not os.path.exists(METRICS_DATA_FILE):
        return None

    # Initially set as true to initiate new sub-dict
    user_data_complete = True

    # Read from file
    data = {}
    player_count = 0
    with open(METRICS_DATA_FILE, 'r') as f:
        for line in f.readlines():
            # Create user sub-dict
            if user_data_complete:
                player_id = 'Player_{}'.format(player_count)
                data[player_id] = {key: [] for key in metrics_keys}

                user_data_complete = False
                player_count += 1

            # Get data
            line = line.split(':')
            user, metric, value = line

            # Convert to proper data type if necessary
            value = convertDataType(value)

            # Grab date and time played - this metric marks the end of
            # a user's session, so mark user_data_complete and set
            # the user name. Otherwise, it's a regular metric.
            if metric == 'DateTime':
                data[player_id][metric].append(value)
                data[player_id]['User'].append(user)
                user_data_complete = True
            else:
                data[player_id][metric].append(value)

    return data

def convertDataType(value):
    """
    Make sure that the value read from the
    file is converted to its correct type
    """
    try:
        return int(value)
    except ValueError:
        return value.replace(';',':').rstrip()


def getMaxLevels(data):
    """ Find the highest number of levels played """
    return max([ int(data[user]['LevelsPlayed'][0]) for user in data.keys() ])
