# Copyright (C) 2018 Nicholas Rosbrook
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#

from enigma_metrics import GenerateMetrics, METRICS_DATA_FILE

from tkFont import Font
from ttk import Progressbar
import Tkinter as tk

import os
import sys
import datetime
import math

# Fix for threading issue in level_start.cc
if not hasattr(sys, 'argv'):
        sys.argv  = ['']

# Return codes
RET_ERR = -1
RET_CONTINUE = 0
RET_EXIT = 1

class LevelStart(object):

    def __init__(self, master, levels_played):
        self.master = master

        # Assume status is 'Continue'
        self.status = RET_CONTINUE

        # Populate UI
        self._addInitialWidgets()

        # Initialize counter and target
        self._levels_played = levels_played
        self._limit = self._getKeyPressLimit()
        self._counter = self._limit

        # Start a timer to measure how long it takes a player
        # to complete the key-pressing requirement
        self._time_start = datetime.datetime.now()

    def setUserName(self, user_name):
        """ Set the user name """
        self.user_name = user_name

    def _getKeyPressLimit(self):
        """ Calculate key press limit """
        limit = 10*((1.3)**self._levels_played)
        return int(math.ceil(limit))

    def _addInitialWidgets(self):
        """ Populate the initial UI """
        self.master.title('Enigma Pre-level')
        self.master.geometry('500x300')
        self.master.configure(background='white')

        # Font configuration
        self.font = Font(family='Helvetica', size=14, weight='bold')

        # Create the label
        self.label = tk.Label(self.master,
                              text='To continue to the next level, repeatedly press spacebar.',
                              font=self.font)
        self.label.pack(pady=25)

        # Add progress bar
        self.p_var = tk.DoubleVar()
        self.p = Progressbar(self.master,
                             orient=tk.HORIZONTAL,
                             variable=self.p_var,
                             length=200,
                             max=100)
        self.p.pack(pady=25)

        # Add the 'Quit Enigma' button
        self.button_label = tk.Label(self.master, text='Or, quit enigma now.')
        self.button = tk.Button(self.master, text='Quit Enigma',
                                command=self._quit, font=self.font)
        self.button_label.pack(pady=15)
        self.button.pack()

        # Bind the space bar to this UI
        self.master.bind('<space>', self._eventSpacebar)

        # Turn off auto-repeat behavior so user cannot hold down spacebar
        os.system('xset r off')

    def _eventSpacebar(self, event):
        """ Spacebar event """
        self._counter -= 1
        if self._counter > 0:
            progress = 100*((self._limit - self._counter)/float(self._limit))
            self.p_var.set(progress)
            self.master.update_idletasks()
        else:
            # User successfully completed the key press requirement,
            # so record the time in the data file
            self._recordTimeElapsed()
            self._close()

    def _recordTimeElapsed(self):
        """
        Record the time take to complete the key
        pressing requirement
        """
        time_end = datetime.datetime.now()
        time_elapsed = time_end - self._time_start

        # Record the time in seconds
        time_elapsed = time_elapsed.seconds

        # Write to data file
        with open(METRICS_DATA_FILE, 'a+') as f:
            f.write('{}:KeypressTime:{}\n'.format(self.user_name, time_elapsed))

    def _quit(self):
        """ Set status to 'Exit' """
        self.status = RET_EXIT
        self._close()

    def _close(self):
        """ Clean up  """
        self.master.destroy()

        # Set autorepeat back on
        os.system('xset r on')

def run(levels_played, user_name):
    """ Run the level entry feature """
    root = tk.Tk()
    ls = LevelStart(root, levels_played)
    ls.setUserName(user_name)
    root.mainloop()

    return ls.status
