# Copyright (C) 2018 Nicholas Rosbrook
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#

from enigma_metrics import GenerateMetrics, METRICS_DATA_FILE

from Tkinter import Tk, Label, Button, Entry, Frame
from tkFont import Font

import os
import sys

# Fix for threading issue in rating.cc
if not hasattr(sys, 'argv'):
        sys.argv  = ['']

class RatingPrompt(object):

    def __init__(self, master):
        self.rating = -1
        self.response = None
        self.master = master

        # Populate UI
        self._addInitialWidgets()

    def setUserName(self, user_name):
        """ Set the user name """
        self.user_name = user_name

    def _addInitialWidgets(self):
        """ Populate the initial UI """
        self.master.title('Enigma Game Rating')
        self.master.geometry('500x400')
        self.master.configure(background='white')

        # Font configuration
        self.font = Font(family='Helvetica', size=14, weight='bold')

        # Create the label
        self.label_rating = Label(self.master,
                                  text='Please rate your enjoyment of this level from 1-100',
                                  font=self.font)
        self.label_rating.pack(pady=10)

        # Add rating entry to the UI
        self.e_rating = Entry(self.master)
        self.e_rating.configure(font=Font(family='FreeSans', size='14',
                                   weight='normal'))
        self.e_rating.pack(padx=25, pady=25)

        # Add widgets for difficulty rating
        self.label_difficulty = Label(self.master,
                                      text='Please rate the difficulty of this level from 1-100',
                                      font=self.font)
        self.label_difficulty.pack(pady=10)

        # Add difficulty entry to the UI
        self.e_difficulty = Entry(self.master)
        self.e_difficulty.configure(font=Font(family='FreeSans', size='14',
                                   weight='normal'))
        self.e_difficulty.pack(padx=25, pady=25)

        # Add button with callback '_processInputs'
        self.button = Button(self.master, text='Submit',
                             command=self._processInputs,
                             font=self.font)
        self.button.pack(padx=25, pady=25)

    def _processInputs(self):
        """ Process the rating """

        # Remove label if it exists
        if self.response and self.response.winfo_exists():
            self.response.destroy()

        try:
            # Get submissions from entries
            self.rating = int(self.e_rating.get())
            self.difficulty = int(self.e_difficulty.get())

            if not (self.rating >= 1 and self.rating <= 100):
                self.response = Label(self.master,text='Please enter a number from 1-100!')

            elif not (self.difficulty >=1 and self.difficulty <= 100):
                self.response = Label(self.master,text='Please enter a number from 1-100!')

            else:
                # Rating is valid!
                self._clean()
                self._saveRating()

        except ValueError:
            self.response = Label(self.master,
                                  text='Please enter a valid number!')

    def _saveRating(self):
        """ Write the rating to data file """
        with open(METRICS_DATA_FILE, 'a+') as f:
            f.write('{}:Rating:{}\n'.format(self.user_name,self.rating))
            f.write('{}:Difficulty:{}\n'.format(self.user_name,
                                                self.difficulty))

    def _close(self):
        """ Close the window """
        self.master.destroy()

    def _clean(self):
        """ Clean up the UI after submission """
        self.e_rating.destroy()
        self.e_difficulty.destroy()
        self.label_rating.destroy()
        self.label_difficulty.destroy()
        self.button.destroy()
        self._close()

def run(user_name):
    """ Run the prompt thread """
    root = Tk()
    rating_prompt = RatingPrompt(root)
    rating_prompt.setUserName(user_name)
    root.mainloop()
