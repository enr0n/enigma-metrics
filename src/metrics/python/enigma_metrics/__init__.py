# Copyright (C) 2018 Nicholas Rosbrook
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#

import os
import platform

# Get path names based on OS
if platform.system() == 'Windows':
    # Fuck windows
    METRICS_DATA_FILE = 'C:{}\\EnigmaMetrics\\metrics_data.txt'.format(os.getenv('HOMEPATH'))
    METRICS_WORKBOOK = 'C:{}\\EnigmaMetrics\\EnigmaData.xlsx'.format(os.getenv('HOMEPATH'))
else:
    METRICS_DATA_FILE = os.path.join(os.getenv('HOME'), '.enigma_metrics/metrics_data.txt')
    METRICS_WORKBOOK = os.path.join(os.getenv('HOME'), '.enigma_metrics/EnigmaData.xlsx')

