from setuptools import setup

setup(
    name='Enigma Metrics',
    version='1.6.0',
    description='Python modules used in metrics extensions of Enigma',
    author='Nicholas Rosbrook',
    packages=['enigma_metrics'],
    install_requires=[
        'xlsxwriter',
    ],
)
