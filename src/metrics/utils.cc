/*
 * Copyright (C) 2018 Nicholas Rosbrook
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */
#include <stdlib.h>
#include <sstream>
#include <string>
#include <ctime>
#include <chrono>
#include <iomanip>

#ifdef WIN32
#include <windows.h>
#endif

namespace metrics {

/* Used to measure elapsed time of trial session */
static std::chrono::time_point<std::chrono::system_clock> t_start, t_end;

/* Check if the level being loaded is new */
static bool new_level = true;

int resetUserData(std::string user_data_dir) {
    int status;

    /* Remove the enigma data directory */
#ifdef WIN32
    status = RemoveDirectory(user_data_dir);
#else
    status = system(("rm -rf " + user_data_dir).c_str());
#endif

    return status;
}

std::string getDateTime() {
    /* Get the current date time */
    time_t rawtime;
    struct tm * timeinfo;
    char buffer[80];

    time (&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer,sizeof(buffer),"%m-%d-%Y %I;%M",timeinfo);
    return std::string(buffer);
}

void startTimer() {
    t_start = std::chrono::high_resolution_clock::now();
}

void stopTimer() {
    t_end = std::chrono::high_resolution_clock::now();
}

std::string getElapsedTime() {
    double t_elapsed = std::chrono::duration< double, std::ratio<60> >(t_end - t_start).count();

    /* Format this as a string - MM:SS */
    long seconds = (int) 60*(t_elapsed - (long)t_elapsed);
    long minutes = (int) t_elapsed;

    std::ostringstream temp;
    temp <<  minutes << ";" << std::setw(2) << std::setfill('0') << seconds;
    return temp.str();
}

bool getIsNewLevel() {
    return new_level;
}

void setIsNewLevel(bool isNewLevel) {
    new_level = isNewLevel;
}
}//metrics
